### [1.1.1](https://gitlab.com/ujlbu4/gitlab-semantic-release/compare/1.1.0...1.1.1) (2020-08-28)


### Bug Fixes

* disable success/fail jobs ([298fb05](https://gitlab.com/ujlbu4/gitlab-semantic-release/commit/298fb055b6b2729fa8b263e51939e66261098be2))

## [1.1.0](https://gitlab.com/ujlbu4/gitlab-semantic-release/compare/1.0.0...1.1.0) (2020-08-28)


### Features

* add notifications ([92da5cd](https://gitlab.com/ujlbu4/gitlab-semantic-release/commit/92da5cdf3efaf6189bd90d5f30370ca2fb5b44d2))

## [1.0.0](https://gitlab.com/ujlbu4/gitlab-semantic-release/compare/...1.0.0) (2020-08-28)


### Bug Fixes

* fake release (test release) ([e8599cb](https://gitlab.com/ujlbu4/gitlab-semantic-release/commit/e8599cbed61aa5cf289a75fed710ce3d0ddff0f1))
